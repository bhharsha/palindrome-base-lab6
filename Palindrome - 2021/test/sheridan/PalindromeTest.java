package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindromeRegular( ) {
		boolean isPalindrome = Palindrome.isPalindrome("racecar");
		assertTrue("Not able to validate palidrome", isPalindrome);
	}

	@Test
	public void testIsPalindromeException( ) {
		boolean isPalindrome = Palindrome.isPalindrome("myself");
		assertFalse("Not able to validate palidrome", isPalindrome);
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		boolean isPalindrome = Palindrome.isPalindrome("Race Car");
		assertTrue("Not able to validate palidrome", isPalindrome);
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		boolean isPalindrome = Palindrome.isPalindrome("Race a Car");
		assertFalse("Not able to validate palidrome", isPalindrome);
	}
}
